extends Button


func _ready():
	connect("button_down", self, "_on_button_down")


func _on_button_down():
	get_tree().quit()
