extends Node


export(String) var to

const WATER_SOUND = preload('res://res/audio/water.mp3')
const ROCK_SOUND = preload('res://res/audio/pebbles.mp3')
const BONES_SOUND = preload('res://res/audio/bones.mp3')
const DIG_1_SOUND = preload('res://res/audio/dig_1.mp3')
const DIG_2_SOUND = preload('res://res/audio/dig_2.mp3')
const DIG_SOUNDS = [DIG_1_SOUND, DIG_2_SOUND]
const TREE_SPRITE_1 = preload('res://res/sprites/tree_1.png')
const TREE_SPRITE_2 = preload('res://res/sprites/tree_2.png')
const TREE_SPRITE_3 = preload('res://res/sprites/tree_3.png')
const TREE_SPRITE_4 = preload('res://res/sprites/tree_4.png')
const TREE_SPRITES = [TREE_SPRITE_1, TREE_SPRITE_2, TREE_SPRITE_3, TREE_SPRITE_4]

const ROOT = 21
const WATER_LEFT = 15
const WATER_RIGHT = 16
const ROCK = 17
const BONE = 18

const POINT_LEFT = Vector2(2,0)
const POINT_RIGHT = Vector2(1,0)
const POINT_UP = Vector2(0,0)
const POINT_DOWN = Vector2(3,0)
const BODY_VERTICAL = Vector2(4,0)
const BODY_HORIZONTAL = Vector2(4,1)
const BODY_LEFT_DOWN = Vector2(1,1)
const BODY_RIGHT_DOWN = Vector2(0,1)
const BODY_LEFT_UP = Vector2(3,1)
const BODY_RIGHT_UP = Vector2(2,1)
const DIR_LEFT = Vector2(-1,0)
const DIR_RIGHT = Vector2(1,0)
const DIR_UP = Vector2(0,-1)
const DIR_DOWN = Vector2(0,1)

var current_root_point = POINT_DOWN
var apple_position

var initial_point = [Vector2(8, 20), Vector2(8, 19)]
var active_branch = [Vector2(8, 20), Vector2(8, 19)]
var root_direction = Vector2(0,1)
var last_direction = Vector2(0,1)
var counter = 0


func _ready():
	pass


func place_items():
	randomize()
	var x = randi() % 26
	var y = (randi() % 32) + 15
	return Vector2(x,y)


func draw_items():
	$RootsAndElements.set_cell(apple_position.x, apple_position.y, WATER_LEFT)


func draw_root():
	if root_direction == last_direction:
		if root_direction == DIR_LEFT or root_direction == DIR_RIGHT:
			$RootsAndElements.set_cell(active_branch[1].x, active_branch[1].y, ROOT, false, false, false, BODY_HORIZONTAL)
		if root_direction == DIR_UP or root_direction == DIR_DOWN:
			$RootsAndElements.set_cell(active_branch[1].x, active_branch[1].y, ROOT, false, false, false, BODY_VERTICAL)
	elif (last_direction == DIR_LEFT and root_direction == DIR_DOWN) or (last_direction == DIR_UP and root_direction == DIR_RIGHT):
		$RootsAndElements.set_cell(active_branch[1].x, active_branch[1].y, ROOT, false, false, false, BODY_RIGHT_DOWN)
	elif (last_direction == DIR_RIGHT and root_direction == DIR_DOWN) or (last_direction == DIR_UP and root_direction == DIR_LEFT):
		$RootsAndElements.set_cell(active_branch[1].x, active_branch[1].y, ROOT, false, false, false, BODY_LEFT_DOWN)
	elif (last_direction == DIR_RIGHT and root_direction == DIR_UP) or (last_direction == DIR_DOWN and root_direction == DIR_LEFT):
		$RootsAndElements.set_cell(active_branch[1].x, active_branch[1].y, ROOT, false, false, false, BODY_LEFT_UP)
	elif (last_direction == DIR_LEFT and root_direction == DIR_UP) or (last_direction == DIR_DOWN and root_direction == DIR_RIGHT):
		$RootsAndElements.set_cell(active_branch[1].x, active_branch[1].y, ROOT, false, false, false, BODY_RIGHT_UP)
	
	if root_direction == DIR_RIGHT: current_root_point = POINT_RIGHT
	if root_direction == DIR_LEFT: current_root_point = POINT_LEFT
	if root_direction == DIR_UP: current_root_point = POINT_UP
	if root_direction == DIR_DOWN: current_root_point = POINT_DOWN
	$RootsAndElements.set_cell(active_branch[0].x, active_branch[0].y, ROOT, false, false, false, current_root_point)
	last_direction = root_direction


func grow_root():
	var body_copy = active_branch.slice(0, active_branch.size() - 2)
	var new_head = body_copy[0] + root_direction
	if $RootsAndElements.get_cell(new_head.x, new_head.y) == ROCK:
		$Player.set_volume_db(0)
		$Player.stream = self.ROCK_SOUND
		$Player.play()
		$Tick.wait_time = $Tick.wait_time - 0.05
	if $RootsAndElements.get_cell(new_head.x, new_head.y) == BONE:
		$Player.set_volume_db(0)
		$Player.stream = self.BONES_SOUND
		$Player.play()
		$Tick.wait_time = $Tick.wait_time + 0.05
	if $RootsAndElements.get_cell(new_head.x, new_head.y) == WATER_LEFT or $RootsAndElements.get_cell(new_head.x, new_head.y) == WATER_RIGHT:
		$Player.set_volume_db(0)
		$Player.stream = self.WATER_SOUND
		$Player.play()

		if counter > 3:
			get_tree().change_scene(to)
		else:
			$Tree.set_texture(TREE_SPRITES[counter])
			counter += 1
			spawn_root()
	elif $RootsAndElements.get_cell(new_head.x, new_head.y) == ROOT:
		reset()
	else:
		if !$Player.is_playing():
			randomize()
			$Player.set_volume_db(-1)
			$Player.stream = self.DIG_SOUNDS[(randi() % 2) - 1]
			$Player.play()
		body_copy.insert(0, new_head)
		active_branch = body_copy


func check_out_of_bounds():
	var root_head = active_branch[0]
	if root_head.x > 26 or root_head.x < 0 or root_head.y > 47 or root_head.y < 20 :
		reset()


func reset():
	clear_screen()
	$Tree.set_texture(null)
	$Tick.wait_time = 0.5
	active_branch = [Vector2(8, 20), Vector2(8, 19)]
	root_direction = Vector2(0,1)
	last_direction = null
	initial_point = [Vector2(8, 20), Vector2(8, 19)]
	counter = 0
	place_items()


func clear_screen():
	var root_cells = $RootsAndElements.get_used_cells_by_id(ROOT)
	for root_cell in root_cells:
		$RootsAndElements.set_cell(root_cell.x, root_cell.y, -1)


func spawn_root():
	initial_point[0] = initial_point[0] + Vector2(2, 0)
	initial_point[1] = initial_point[1] + Vector2(2, 0)
	active_branch = initial_point
	root_direction = Vector2(0,1)
	last_direction = root_direction


func delete_tiles(id: int):
	var cells = $RootsAndElements.get_used_cells_by_id(id)
	for cell in cells:
		$RootsAndElements.set_cell(cell.x, cell.y, -1)


func _input(_event):
	if Input.is_action_just_pressed("ui_up"):
		if last_direction == DIR_DOWN:
			reset()
			return
		root_direction = DIR_UP
	if Input.is_action_just_pressed("ui_right"):
		if last_direction == DIR_LEFT:
			reset()
			return
		root_direction = DIR_RIGHT
	if Input.is_action_just_pressed("ui_left"):
		if last_direction == DIR_RIGHT:
			reset()
			return
		root_direction = DIR_LEFT
	if Input.is_action_just_pressed("ui_down"):
		if last_direction == DIR_UP:
			reset()
			return
		root_direction = DIR_DOWN


func _on_Tick_timeout():
	check_out_of_bounds()
	grow_root()
	draw_root()
